![Screenshot](images/menu/bg_complet.jpg)

Dans le cadre de notre première année de licence en mathématiques informatique à l'université Clermont Auvergne. Il nous est proposé de réaliser un projet intitulé 《la réussite des alliances 》de 3 mois nous permettant de mettre en pratique nos connaissances et nos compétences professionnelles python et ses différentes bibliothèques
## Pour commencer

Placez vous dans le dossier du projet avec la commande cd

### Pré-requis

Ce qu'il est requis pour lancer le jeu ...

- Une version récente de Python (3 ou plus)
- Pygame
- Matplotlib

### Installation

1 - Lancer un terminal dans le dossier du projet
Executez la commande `python 3 game.py`, cette derniere lancera le jeu et il vous restera qu'a l’essayer.

## Fabriqué avec

- [python](https://docs.python.org/3/) - Langage de programmation interprété, multi-paradigme et multiplateformes
- [pygame](https://www.pygame.org/docs/) - ne bibliothèque libre multiplate-forme qui facilite le développement de jeux vidéo temps réel avec le langage de programmation Python

## Versions

**Dernière version stable :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.isima.fr/mwabichou/la_reussite_des_alliances/-/tags)

## Auteurs

- **Mohamed Wassim Abichou** _alias_ [@mwabichou](https://gitlab.isima.fr/mwabichou)

Lisez la liste des [contributeurs](https://gitlab.isima.fr/mwabichou/la_reussite_des_alliances/-/project_members) pour voir qui à aidé au projet !

## License

Ce projet est sous la licence `AMW`
