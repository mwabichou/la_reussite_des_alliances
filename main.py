import random
from statistics import mode
import matplotlib.pyplot as plt
import sys
import subprocess
import pkg_resources

try:
    import enquiries
except ImportError:
    import pip
    pip.main(['install', '--user', 'enquiries'])
    import enquiries


def carte_to_chaine(carte):
    if carte['valeur'] == 10:
        chaine = str(carte['valeur'])
    else:
        chaine = " " + str(carte['valeur'])

    if carte['couleur'] == "P":
        chaine += chr(9824)
    elif carte['couleur'] == "K":
        chaine += chr(9826)
    elif carte['couleur'] == "C":
        chaine += chr(9825)
    elif carte['couleur'] == "T":
        chaine += chr(9827)
    return chaine


def afficher_reussite(liste_carte):
    for carte in liste_carte:
        print(carte_to_chaine(carte), end=" ")
    print("\n")


def init_pioche_fichier(nom_fich):
    f = open(nom_fich, "r")
    liste_carte = []
    carte = {}
    for ligne in f:
        liste_carte_before = ligne.strip().split(" ")
    for element in liste_carte_before:
        pos_tiret = element.find('-')
        try:
            carte['valeur'] = int(element[:pos_tiret])
        except:
            carte['valeur'] = element[:pos_tiret]
        carte['couleur'] = element[pos_tiret+1:]
        liste_carte.append(carte)
        carte = {}
    return liste_carte


def ecrire_fichier_reussite(nom_fich, liste_carte):
    f = open(nom_fich, "w")

    for carte in liste_carte:
        f.write("%s-%s " % (str(carte['valeur']), carte['couleur']))
    f.close()


def init_pioche_alea(nb_cartes=32):
    jeu_de_cartes = {'32': ['A', 7, 8, 9, 10, 'V', 'D', 'R'], '52': [
        'A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'V', 'D', 'R']}
    couleurs_cartes = ['P', 'K', 'C', 'T']
    liste_carte = []
    carte = {}

    for valeur in jeu_de_cartes[str(nb_cartes)]:
        for couleur in couleurs_cartes:
            carte['valeur'] = valeur
            carte['couleur'] = couleur
            liste_carte.append(carte)
            carte = {}
    random.shuffle(liste_carte)
    return liste_carte


def alliance(carte1, carte2):
    return (carte1['valeur'] == carte2['valeur'] or carte1['couleur'] == carte2['couleur'])


def saut_si_possible(liste_tas, num_tas):
    saut = False
    if num_tas != len(liste_tas)-1 and num_tas != 0:
        saut = alliance(liste_tas[num_tas - 1], liste_tas[num_tas + 1])
    if (saut):
        del liste_tas[num_tas - 1]
    return saut


def une_etape_reussite(liste_tas, pioche, affiche=False):
    saut = False
    liste_tas.append(pioche[0])
    del pioche[0]
    if (affiche):
        afficher_reussite(liste_tas)
    if len(liste_tas) >= 3:
        saut = saut_si_possible(liste_tas, len(liste_tas)-2)
    if (saut and affiche):
        afficher_reussite(liste_tas)
    i = 1
    while i < len(liste_tas)-1:
        saut = saut_si_possible(liste_tas, i)
        if (saut):
            i = 0
        if (saut and affiche):
            afficher_reussite(liste_tas)
        i += 1


def reussite_mode_auto(pioche, affiche=False):
    pioche_copie = list(pioche)
    liste_tas = []
    if affiche:
        afficher_reussite(pioche_copie)
    while len(pioche_copie) > 0:
        une_etape_reussite(liste_tas, pioche_copie, affiche)
    return liste_tas


def aucun_saut_possible(liste_tas):
    saut = False
    if len(liste_tas) == 0:
        return True
    for i in range(1, len(liste_tas)-1):
        saut = alliance(
            liste_tas[i - 1], liste_tas[i + 1])
        if saut:
            return True
    return False


def ext_aide(liste_tas):
    i = 1
    saut = False
    while i < len(liste_tas)-1:
        saut = saut_si_possible(liste_tas, i)
        if saut:
            return saut
        i += 1
    return saut


def menu(nb_pioche, nb_tas, nb_melange, nb_aide):
    if nb_pioche > 0:
        print("[1] Piocher une carte")
    if nb_tas > 2:
        print("[2] Sauter une carte si possible")
        if nb_aide > 0:
            print("[3] Aide : " + str(nb_aide) + "restant(s)")
        if nb_melange > 0:
            print("[4] Melanger les cartes : " +
                  str(nb_melange) + "restant(s)")

    print("[0] Quitter")


def numerotation_paralelle(liste_tas):
    i = 1
    chaine = ""
    for carte in liste_tas:
        chaine += carte_to_chaine(carte) + " "
    for x in range(len(chaine)):
        if chaine[x] == " ":
            print(" ", end="")
        elif chaine[x] == "0" and chaine[x-1] == "1":
            print(" ", end="")
        elif chaine[x-1] == " " and chaine[x+1] != " " and i < 10:
            print("0", end="")
        elif chaine[x-1] != " " and chaine[x+1] == " " and i < 10:
            print(i, end="")
            i += 1
        elif chaine[x-1] == " " and chaine[x+1] != " " and i >= 10:
            print(i, end="")
            i += 1
    print()


def reussite_mode_manuel(pioche, nb_tas_max=2):
    pioche_copie = list(pioche)
    liste_tas = []
    saut_restant = True
    nb_aide = 5
    nb_melange = 3
    gagne = True
    while len(pioche_copie) > 0 or saut_restant:
        menu(len(pioche_copie), len(liste_tas), nb_melange, nb_aide)
        rep = int(input("Votre choix ? "))
        if (rep == 0):
            break
        if (rep == 1) and len(pioche_copie) > 0:
            liste_tas.append(pioche_copie[0])
            del pioche_copie[0]
            afficher_reussite(liste_tas)
        elif (rep == 2) and len(liste_tas) > 2:
            afficher_reussite(liste_tas)
            numerotation_paralelle(liste_tas)
            pos_saut = int(input("Vous voulez sauter quelle carte ?: "))
            try:
                saut = saut_si_possible(liste_tas, pos_saut-1)
                if (not(saut)):
                    print("oupps saut de carte pas possible a cette position !")
            except:
                print("Erreur : position de carte invalide !")
            afficher_reussite(liste_tas)
        elif (rep == 3) and nb_aide > 0:
            if ext_aide(liste_tas):
                nb_aide -= 1
                afficher_reussite(liste_tas)
            else:
                print("pas de saut possible\n")
        elif (rep == 4) and nb_aide > 0:
            random.shuffle(pioche_copie)
            nb_melange -= 1
            print("pioche mélangée\n")
        if len(pioche_copie) == 0:
            saut_restant = aucun_saut_possible(liste_tas)
    if (len(liste_tas) > nb_tas_max or rep == 0):
        print(len(liste_tas))
        print(nb_tas_max)
        print("Vous avez perdu")
        gagne = False
    if rep == 0 and not(gagne):
        liste_tas.extend(pioche_copie)
        print("Tas de cartes restants : ")
        afficher_reussite(liste_tas)
        print("Aurevoir Looser !")
    if (gagne):
        print("Ohhh tu as gagner ! mais pas la prochaine fois !")
        print("Aurevoir Winner !")


def lance_reussite(mode, nb_cartes=32, nb_tas_max=2, affiche=False):
    pioche = init_pioche_alea(nb_cartes)
    if mode == 'manuel' and verifier_pioche(pioche, nb_cartes):
        reussite_mode_manuel(pioche, nb_tas_max)
    elif mode == 'auto' and verifier_pioche(pioche, nb_cartes):
        reussite_mode_auto(pioche, affiche)


def verifier_pioche(tas_carte, nb_cartes=32):
    jeux_de_carte = init_pioche_alea(nb_cartes)
    tas_carte_copie = list(tas_carte)
    try:
        for carte in tas_carte_copie:
            jeux_de_carte.remove(carte)
    except ValueError:
        return False
    return not jeux_de_carte


def res_multi_simulation(nb_sim, nb_cartes=32):
    reste_tas = []
    for _ in range(nb_sim):
        liste_tas = init_pioche_alea(nb_cartes)
        reste_tas.append(len(reussite_mode_auto(liste_tas)))
    return reste_tas


def statistiques_nb_tas(nb_sim, nb_cartes=32):
    liste_nb_tas_restant = res_multi_simulation(nb_sim, nb_cartes)
    moyenne = sum(liste_nb_tas_restant)/len(liste_nb_tas_restant)
    print("Le minimum de nombre de tas obtenu :", min(liste_nb_tas_restant))
    print("Le maximum de nombre de tas obtenu :", max(liste_nb_tas_restant))
    print("Le nombre de fois ou il reste que 2 tas :",
          liste_nb_tas_restant.count(2))
    print("Le moyenne de nombre de tas obtenu :", moyenne)


def graphique_probabilite(nb_cartes=32, nb_simulations=100):
    liste_nb_tas_max = [x for x in range(2, nb_cartes+1)]
    liste_probabilite = []
    for nb_tax_max in liste_nb_tas_max:
        liste_probabilite.append(
            res_multi_simulation(nb_simulations, nb_cartes).count(nb_tax_max))

    plt.rcParams["figure.figsize"] = (25, 10)
    plt.bar(liste_nb_tas_max, liste_probabilite,
            label="nombre de tas maximum", color="#62bfed", ec="black")
    plt.axhline(y=14, color="#fbcaef",
                label="Moyenne de tas restant", linestyle='--')

    plt.title("La probabilité de gagner", fontweight="bold",
              backgroundcolor="#FBCAEF", fontsize="30", pad="20", color="white")
    plt.xlabel("Nombre de tas max", fontweight="bold")
    plt.ylabel("probabilté de gagner", fontweight="bold")
    plt.grid(True, alpha=0.7)
    # plt.legend()
    plt.savefig('probabilite/graphique.png', transparent=False)
    # plt.show()


def verif_saut(pioche, i):
    saut = False
    try:
        if alliance(pioche[i], pioche[i+2]):
            saut = True
    except:
        pass
    if alliance(pioche[i], pioche[i-2]) or saut:
        return True
    return False


def meilleur_echange_consecutif(pioche):
    pioche_copie = list(pioche)
    res_echange = len(reussite_mode_auto(pioche_copie))
    pos_echange = 0
    for i in range(len(pioche_copie)-1):
        aux = pioche_copie[i]
        pioche_copie[i] = pioche_copie[i+1]
        pioche_copie[i+1] = aux
        if len(reussite_mode_auto(pioche_copie)) <= res_echange:
            res_echange = len(reussite_mode_auto(pioche_copie))
            pos_echange = i
        pioche_copie = list(pioche)

    pioche_copie = list(pioche)
    aux = pioche_copie[pos_echange]
    pioche_copie[pos_echange] = pioche_copie[pos_echange+1]
    pioche_copie[pos_echange+1] = aux
    return pioche_copie,  len(reussite_mode_auto(pioche))-len(reussite_mode_auto(pioche_copie))


def find_pos_alliance(carte, pioche, offset):
    i = 0
    trouve = False
    for c in pioche[offset+1:]:
        if carte == c:
            pos = i
            trouve = True
        i += 1
    if trouve:
        return pos


def menu_principale():
    options = ['Reussite mode graphique UI',
               'Reussite mode auto', 'Reussite mode manuel', 'Quitter']
    choix = enquiries.choose(
        'Bienvenue dans la reussite des alliances: (les fleches pour naviguer)', options)
    if choix == 'Quitter':
        sys.exit()
    if choix == 'Reussite mode graphique UI':
        import game
        game.main_menu()
    if choix == 'Reussite mode auto':
        options = ['32', '52']
        nb_cartes = int(enquiries.choose(
            'Nombre de cartes dans la pioche ?', options))
        options = ['oui', 'non']
        affiche = enquiries.choose(
            'Afficher la reussite ?', options)
        if affiche == 'oui':
            lance_reussite("auto", nb_cartes, affiche=True)
            menu_principale()
        else:
            lance_reussite(nb_cartes)
            menu_principale()
    if choix == 'Reussite mode manuel':
        options = ['32', '52']
        nb_cartes = int(enquiries.choose(
            'Nombre de cartes dans la pioche ?', options))
        options = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '15']
        nb_tas_max = int(enquiries.choose(
            'Nombre de tas max ?', options))
        print(nb_tas_max)
        lance_reussite("manuel", nb_cartes, nb_tas_max)
        menu_principale()


if __name__ == "__main__":
    menu_principale()
