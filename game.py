import time
import os
from tkinter import Y
from matplotlib.pyplot import cla
import pygame
from pygame.locals import *
from pygame import image, init
from pygame.constants import MOUSEBUTTONDOWN
from pygame.display import update
from pygame.event import get, wait
from pygame.time import delay
import main
import random
from pygame import mixer
import sys
import webbrowser


# initialisation
MAIN_FILE_PATH = os.path.dirname(__file__)
IMAGES_FOLDER = os.path.join(MAIN_FILE_PATH, "images")
MENU_FOLDER = os.path.join(IMAGES_FOLDER, "menu")
SOUNDS_FOLDER = os.path.join(MAIN_FILE_PATH, "sounds")
CARDS_FOLDER = os.path.join(IMAGES_FOLDER, "cards")
GRAPHIQUE_FOLDER = os.path.join(MAIN_FILE_PATH, "probabilite")
lien_cartes = [f for f in os.listdir(CARDS_FOLDER)]
pygame.font.init()
pygame.display.set_caption("La reussite Des Alliances")
icon = pygame.image.load(os.path.join(
    IMAGES_FOLDER, "icone.png"))
pygame.display.set_icon(icon)
LARGEUR, HAUTEUR = 1440, 900
# 800, 500 bonne resolution aussi
FENETRE = pygame.display.set_mode(
    (0, 0), pygame.FULLSCREEN, pygame.RESIZABLE)
x_ecran, y_ecran = FENETRE.get_size()
# importation images
BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(IMAGES_FOLDER, "bg.jpg")), (x_ecran, y_ecran)).convert_alpha()
PERDU_BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(IMAGES_FOLDER, "perdu.jpg")), (x_ecran, y_ecran)).convert_alpha()
GAGNE_BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(IMAGES_FOLDER, "gagne.jpg")), (x_ecran, y_ecran)).convert_alpha()
REGLES_BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(IMAGES_FOLDER, "regles_du_jeu.jpg")), (x_ecran, y_ecran)).convert_alpha()
CLASSMENT_BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(IMAGES_FOLDER, "bg_classement.jpg")), (x_ecran, y_ecran)).convert_alpha()
MENU_BG = pygame.transform.smoothscale(pygame.image.load(
    os.path.join(MENU_FOLDER, "MENU_bg.jpg")), (x_ecran, y_ecran)).convert_alpha()

# musqiue et son
pygame.mixer.init()
BG_MUSIC = mixer.music.load(os.path.join(SOUNDS_FOLDER, "bgmusic1.mp3"))
mixer.music.play(-1)
mixer.music.set_volume(0.02)
# font initialisation
small_text = pygame.font.Font('freesansbold.ttf', 25)
# changement de curseur
pygame.mouse.set_cursor(*pygame.cursors.tri_left)
# initialisation variable
game_pack = []
dir_carte = " "
cartes_tire = []
nb_melanger = 3
nb_son = 1
nb_aide = 5
i = 0
x_cartes = 80
OFFSET = 80
TAILLE_CARTE = (70, 100)


def gamePack(nb_cartes=32):
    global game_pack, dir_carte, cartes_tire, x_cartes, nb_melanger, nb_aide
    game_pack = init_pioche_alea_classes(nb_cartes)
    dir_carte = " "
    cartes_tire = []
    nb_melanger = 3
    nb_aide = 5
    i = 0
    x_cartes = 80

    return game_pack


def cards_display_pack():
    global game_pack, dir_carte
    dir_carte = "in"
    for carte in game_pack:
        carte.new_x = 50
        carte.new_y = 50


def cards_display():
    global game_pack, dir_carte
    dir_carte = "out"
    x_cartes = 80
    for carte in cartes_tire:
        carte.x = x_cartes
        carte.new_y = 390
        x_cartes += 40


def cards_shift():
    global x_cartes
    cartes_tire[0].x = 40
    carte_aux = cartes_tire[0]
    i = 1
    for carte in cartes_tire:
        if carte == cartes_tire[0]:
            carte.new_x = 40
            carte.x = 40
        elif carte_aux.new_x - carte.new_x != -1*OFFSET or carte_aux.x - carte.x != 1*OFFSET:
            carte.new_x = carte_aux.new_x + OFFSET
            carte.x = carte_aux.x + OFFSET
        carte_aux = carte
        i += 1
        x_cartes = cartes_tire[len(cartes_tire)-1].new_x + OFFSET


def card_display():
    global game_pack, cartes_tire, dir_carte, x_cartes
    dir_carte = "out"
    cartes_tire.append(game_pack[0])
    if cartes_tire[len(cartes_tire)-1].x == 50 and cartes_tire[len(cartes_tire)-1].y == 50:
        cartes_tire[len(cartes_tire)-1].new_x = x_cartes
        cartes_tire[len(cartes_tire)-1].new_y = 450/900*y_ecran
        card_sound = mixer.Sound(os.path.join(SOUNDS_FOLDER, "Card-flip.mp3"))
        card_sound.set_volume(0.5)
        card_sound.play()
        del game_pack[0]
    x_cartes += OFFSET


class BUTTON:
    def __init__(self, x, y, image, scale=1):
        self.scale = scale
        self.width = image.get_width()*self.scale
        self.height = image.get_height()*self.scale
        self.sound = mixer.Sound(os.path.join(
            SOUNDS_FOLDER, "retro_button.wav"))
        self.sound.set_volume(0.5)
        self.x = x
        self.y = y
        self.image = pygame.transform.smoothscale(
            image, (int(self.width), int(self.height))).convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.pressed = 0

    def affiche(self, window):
        window.blit(self.image, (self.x, self.y))

    def drag(self, window):
        mouse_pos = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()
        if self.x + self.width > mouse_pos[0] > self.x and self.y + self.height > mouse_pos[1] > self.y:
            if mouse_click[0] == 1:
                self.x = mouse_pos[0]-200
                self.y = mouse_pos[1]-200

    def click(self, window):
        mouse_pos = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()
        if self.x + self.width > mouse_pos[0] > self.x and self.y + self.height > mouse_pos[1] > self.y:
            window.blit(self.image, (self.rect.x, self.rect.y))
            if mouse_click[0] == 1 and self.pressed == 0:
                self.pressed = 1
                return True
        elif mouse_click == (0, 0, 0):
            self.image = pygame.transform.smoothscale(
                self.image, (self.width, self.height)).convert_alpha()
            self.pressed = 0
        return False


class CARTE:
    def __init__(self, dict, new_x=50, new_y=50, x=50, y=50, pos=None):
        self.valeur = str(dict['valeur'])
        self.couleur = dict['couleur']
        self.dict = dict
        self.pos = pos
        self.image = pygame.transform.smoothscale(pygame.image.load(
            os.path.join(CARDS_FOLDER, "carte-"+str(self.valeur)+"-"+self.couleur+".png")).convert_alpha(), TAILLE_CARTE)
        self.x = x
        self.y = y
        self.rect = self.image.get_rect()
        self.new_x = new_x
        self.new_y = new_y
        self.rect.topleft = (self.new_x, self.new_y)
        self.pressed = 0

    def affiche(self, window):
        window.blit(self.image, (self.x, self.y))

    def move(self, vel, direction):
        if direction == "out":
            if self.new_x > self.x and self.new_y > self.y:
                self.x += vel
                self.y += vel
            elif self.new_y != self.y:
                self.y += vel
            elif self.new_x != self.x:
                self.x += vel
        if direction == "in":
            if self.new_x != self.x and self.new_y != self.y:
                self.x -= vel
                self.y -= vel
            elif self.new_x != self.x:
                self.x -= vel
            elif self.new_y != self.y:
                self.y -= vel

    def click(self):
        mouse_pos = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()
        if self.x + TAILLE_CARTE[0] > mouse_pos[0] > self.x and self.y + TAILLE_CARTE[1] > mouse_pos[1] > self.y:
            self.image = pygame.transform.smoothscale(pygame.image.load(
                os.path.join(CARDS_FOLDER, "carte-"+str(self.valeur)+"-"+self.couleur+".png")).convert_alpha(), (73, 103)).convert_alpha()
            if mouse_click[0] == 1 and self.pressed == 0:
                self.pressed = 1
                return True
        elif mouse_click == (0, 0, 0):
            self.image = pygame.transform.smoothscale(pygame.image.load(
                os.path.join(CARDS_FOLDER, "carte-"+str(self.valeur)+"-"+self.couleur+".png")).convert_alpha(), TAILLE_CARTE).convert_alpha()
            self.pressed = 0
        return False


def init_pioche_alea_classes(nb_cartes=32):
    cartes = main.init_pioche_alea(nb_cartes)
    liste_class_cartes = []
    for carte in cartes:
        liste_class_cartes.append(CARTE(carte))
    return liste_class_cartes


def saut_si_possible_classes(liste_tas, num_tas):
    if num_tas != 0 and num_tas != len(liste_tas) - 1:
        saut = main.alliance(
            liste_tas[num_tas - 1].dict, liste_tas[num_tas + 1].dict)
        if (saut):
            card_saut = mixer.Sound(os.path.join(
                SOUNDS_FOLDER, "Card-saut.wav"))
            card_saut.set_volume(0.3)
            card_saut.play()
            del liste_tas[num_tas - 1]
    return saut


def aucun_saut(liste_tas):
    saut = False
    for i in range(1, len(liste_tas)-1):
        saut = main.alliance(
            liste_tas[i - 1].dict, liste_tas[i + 1].dict)
        if saut:
            return True
    return False


quitter_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_quitter.png")).convert_alpha()
logo_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_logo.png")).convert_alpha()
jouer_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_jouer.png")).convert_alpha()
stats_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_stats.png")).convert_alpha()
regles_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_regles.png")).convert_alpha()
classement_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "MENU_classement.png")).convert_alpha()
retour_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_retour.png")).convert_alpha()
image_32 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_32.png")).convert_alpha()
image_52 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_52.png")).convert_alpha()
lancer_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_lancer.png")).convert_alpha()
image_100 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_100.png")).convert_alpha()
image_667 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_667.png")).convert_alpha()
image_999 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_999.png")).convert_alpha()
image_2000 = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_2000.png")).convert_alpha()
mur_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "STATS_mur.png")).convert_alpha()
aide_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_Aide.png")).convert_alpha()
apres_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_apres.png")).convert_alpha()
avant_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_avant.png")).convert_alpha()
melanger_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_melanger.png")).convert_alpha()
mute_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_mute.png")).convert_alpha()
unmute_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_unmute.png")).convert_alpha()
open_menu_image = pygame.image.load(os.path.join(
    MENU_FOLDER, "JEU_open_menu.png")).convert_alpha()
image_2 = pygame.image.load(os.path.join(
    MENU_FOLDER, "PARAM_2.png")).convert_alpha()
image_4 = pygame.image.load(os.path.join(
    MENU_FOLDER, "PARAM_4.png")).convert_alpha()
image_6 = pygame.image.load(os.path.join(
    MENU_FOLDER, "PARAM_6.png")).convert_alpha()
image_8 = pygame.image.load(os.path.join(
    MENU_FOLDER, "PARAM_8.png")).convert_alpha()
image_pack = pygame.image.load(os.path.join(
    MENU_FOLDER, "pack.png")).convert_alpha()
image_pack_vide = pygame.image.load(os.path.join(
    MENU_FOLDER, "pack_vide.png")).convert_alpha()

pack = BUTTON((25/1440)*x_ecran, (13/900) *
              y_ecran, image_pack, 1)
pack_vide = BUTTON((25/1440)*x_ecran, (13/900) *
                   y_ecran, image_pack_vide, 1)
param_2 = BUTTON((310/1440)*x_ecran, (614/900) *
                 y_ecran, image_2, (x_ecran/1440))
param_4 = BUTTON((504/1440)*x_ecran, (614/900) *
                 y_ecran, image_4, (x_ecran/1440))
param_6 = BUTTON((709/1440)*x_ecran, (614/900) *
                 y_ecran, image_6, (x_ecran/1440))
param_8 = BUTTON((916/1440)*x_ecran, (614/900) *
                 y_ecran, image_8, (x_ecran/1440))
aide = BUTTON((312/1440)*x_ecran, (800/900) *
              y_ecran, aide_image, (x_ecran/1440))
apres = BUTTON((1322/1440)*x_ecran, (11/900) *
               y_ecran, apres_image, (x_ecran/1440))
avant = BUTTON((1219/1440)*x_ecran, (11/900) *
               y_ecran, avant_image, (x_ecran/1440))
melanger = BUTTON((5/1440)*x_ecran, (800/900) *
                  y_ecran, melanger_image, (x_ecran/1440))
mute = BUTTON((1272/1440)*x_ecran, (11/900) *
              y_ecran, mute_image, (x_ecran/1440))
unmute = BUTTON((1272/1440)*x_ecran, (11/900) *
                y_ecran, unmute_image, (x_ecran/1440))
open_menu = BUTTON((1375/1440)*x_ecran, (10/900) *
                   y_ecran, open_menu_image, (x_ecran/1440))
quitter = BUTTON((1250/1440)*x_ecran, (784/900) *
                 y_ecran, quitter_image, (x_ecran/1440))
quitter_menu = BUTTON((20/1440)*x_ecran, (784/900) *
                      y_ecran, quitter_image, (x_ecran/1440))
jouer = BUTTON((335/1440)*x_ecran, (495/900) *
               y_ecran, jouer_image, (x_ecran/1440))
stats = BUTTON((705/1440)*x_ecran, (495/900) *
               y_ecran, stats_image, (x_ecran/1440))
regles = BUTTON((523/1440)*x_ecran, (610/900) *
                y_ecran, regles_image, (x_ecran/1440))
classement = BUTTON((523/1440)*x_ecran, (730/900)*y_ecran,
                    classement_image, (x_ecran/1440))
logo = BUTTON((269/1440)*x_ecran, (115/900) *
              y_ecran, logo_image, (x_ecran/1440))
retour = BUTTON((1/1440)*x_ecran, (15/900)*y_ecran,
                retour_image, (x_ecran/1440))
lancer = BUTTON((520/1440)*x_ecran, (730/900) *
                y_ecran, lancer_image, (x_ecran/1440))
mur = BUTTON((285/1440)*x_ecran, (570/900)*y_ecran, mur_image, (x_ecran/1440))
cartes_32 = BUTTON((435/1440)*x_ecran, (470/900) *
                   y_ecran, image_32, (x_ecran/1440))
cartes_52 = BUTTON((793/1440)*x_ecran, (470/900) *
                   y_ecran, image_52, (x_ecran/1440))
stat_100 = BUTTON((310/1440)*x_ecran, (614/900) *
                  y_ecran, image_100, (x_ecran/1440))
stat_667 = BUTTON((504/1440)*x_ecran, (614/900) *
                  y_ecran, image_667, (x_ecran/1440))
stat_999 = BUTTON((709/1440)*x_ecran, (614/900) *
                  y_ecran, image_999, (x_ecran/1440))
stat_2000 = BUTTON((916/1440)*x_ecran, (614/900) *
                   y_ecran, image_2000, (x_ecran/1440))


def statistiques_nb_tas_UI(nb_sim, nb_cartes=32):
    liste_nb_tas_restant = main.res_multi_simulation(nb_sim, nb_cartes)
    moyenne = sum(liste_nb_tas_restant)/len(liste_nb_tas_restant)
    minimum = "Le minimum de nombre de tas obtenu :" + \
        str(min(liste_nb_tas_restant))
    maximum = "Le maximum de nombre de tas obtenu :" + \
        str(max(liste_nb_tas_restant))
    nb_deux = "Le nombre de fois ou il reste que 2 tas :" + \
        str(liste_nb_tas_restant.count(2))
    draw_text(str(minimum), font_nombre,
              (255, 255, 255), FENETRE, (270/1440)*x_ecran, (839/900) *
              y_ecran)
    draw_text(str(maximum), font_nombre,
              (255, 255, 255), FENETRE, (270/1440)*x_ecran, (839/900) *
              y_ecran)
    draw_text(str(nb_deux), font_nombre,
              (255, 255, 255), FENETRE, (270/1440)*x_ecran, (839/900) *
              y_ecran)
    draw_text(str(moyenne), font_nombre,
              (255, 255, 255), FENETRE, (270/1440)*x_ecran, (839/900) *
              y_ecran)


def update_and_wait(delay):
    pygame.display.flip()
    pygame.event.pump()
    pygame.time.delay(delay * 1000)


def waitFor(milliseconds):
    time_now = pygame.time.get_ticks()
    finish_time = time_now + milliseconds

    while time_now < finish_time:

        for event in pygame.event.get():
            if (event.type == pygame.QUIT):

                pygame.event.post(pygame.event.Event(pygame.QUIT))
                break
            elif (event.type == pygame.KEYDOWN):
                if (event.key == pygame.K_ESCAPE):
                    break

        pygame.display.update()
        pygame.time.wait(300)
        time_now = pygame.time.get_ticks()


def une_etape_reussite_classes():
    global x_cartes
    card_display()
    card_display()
    while len(game_pack) > 0:
        card_display()
        saut = saut_si_possible_classes(cartes_tire, len(cartes_tire)-2)
        if saut:
            x_cartes -= 40
            cards_shift()
        i = 1
        while i < len(cartes_tire)-1:
            saut = saut_si_possible_classes(cartes_tire, i)
            if saut:
                x_cartes -= 40
                cards_shift()
                i = 0
            i += 1


def ext_aide():
    i = 1
    saut = False
    while i < len(cartes_tire)-1:
        saut = saut_si_possible_classes(cartes_tire, i)
        if saut:
            cards_shift()
            return saut
        i += 1
    return saut


def find_pos(card):
    i = 0
    for carte in cartes_tire:
        if carte.x == card.x:
            return i
        else:
            i += 1


def reussite_mode_manuel():
    mx, my = pygame.mouse.get_pos()
    cards_clicked = []
    saut = False
    for event in pygame.event.get():
        try:
            if event.type == MOUSEBUTTONDOWN and game_pack[0].click() and len(game_pack) > 0:
                card_display()
        except:
            pass
        if not((mx >= 49 and mx <= 152 and my <= 180 and my >= 49)):
            for card in cartes_tire:
                if card.click() and (len(cartes_tire) > 2):
                    try:
                        saut = saut_si_possible_classes(
                            cartes_tire, find_pos(card))
                    except:
                        pass
                    finally:
                        if saut:
                            cards_shift()
                        else:
                            saut_error = mixer.Sound(os.path.join(
                                SOUNDS_FOLDER, "click_error.wav"))
                            saut_error.set_volume(0.1)
                            saut_error.play()
                            saut_impossible = mixer.Sound(os.path.join(
                                SOUNDS_FOLDER, "saut_impossible.mp3"))
                            saut_impossible.set_volume(0.3)
                            saut_impossible.play()


mainClock = pygame.time.Clock()
pygame.init()
pygame.display.set_caption('MENU PRINCIPALE')
MENU = pygame.display.set_mode((0, 0), pygame.FULLSCREEN, pygame.RESIZABLE)

font = pygame.font.SysFont(None, 20)
font_nombre = pygame.font.SysFont(None, 40)


def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, 1, color)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)


click = False


def main_menu():
    run = True
    while run:

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        MENU.blit(MENU_BG, (0, 0))
        draw_text('menu Principal', font, (255, 255, 255), MENU, 20, 20)
        mx, my = pygame.mouse.get_pos()
        jouer.affiche(MENU)
        stats.affiche(MENU)
        regles.affiche(MENU)
        classement.affiche(MENU)
        quitter_menu.affiche(MENU)
        logo.affiche(MENU)
        if jouer.click(MENU):
            param()
            pygame.quit()
        if stats.click(MENU):
            statistique()
            pygame.quit()
        if regles.click(MENU):
            regles_du_jeu()
            pygame.quit()
        if classement.click(MENU):
            menu_classement()
            pygame.quit()
            # webbrowser.open(
            #     r"https://www.linkedin.com/in/med-wassim-abichou-8b10241b8/")
        if quitter_menu.click(MENU):
            pygame.quit()
            sys.exit()
        logo.drag(MENU)
        pygame.display.update()
        mainClock.tick(60)


def statistique(nb_cartes=32, nb_simulations=100):
    running = True
    while running:
        MENU.blit(MENU_BG, (0, 0))
        draw_text('statistiques', font, (255, 255, 255), MENU, 20, 20)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        lancer.affiche(MENU)
        mur.affiche(MENU)
        cartes_32.affiche(MENU)
        cartes_52.affiche(MENU)
        stat_100.affiche(MENU)
        stat_667.affiche(MENU)
        stat_999.affiche(MENU)
        stat_2000.affiche(MENU)
        retour.affiche(MENU)
        logo.affiche(MENU)
        if quitter_menu.click(MENU):
            pygame.quit()
            sys.exit()
        if retour.click(MENU):
            running = False
            main_menu()
        if cartes_32.click(MENU):
            nb_cartes = 32
        if cartes_52.click(MENU):
            nb_cartes = 52
        if stat_100.click(MENU):
            nb_simulations = 100
        if stat_667.click(MENU):
            nb_simulations = 667
        if stat_999.click(MENU):
            nb_simulations = 999
        if stat_2000.click(MENU):
            nb_simulations = 2000
        if lancer.click(MENU):
            calcul(nb_cartes, nb_simulations)
        pygame.display.update()
        mainClock.tick(60)


def resultat(res):
    running = True
    while running:
        if not res:
            FENETRE.blit(PERDU_BG, (0, 0))
        if res:
            FENETRE.blit(GAGNE_BG, (0, 0))
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        retour.affiche(MENU)
        if quitter_menu.click(MENU):
            running = False
            main_menu()
        if retour.click(MENU):
            param()
        pygame.display.update()
        mainClock.tick(60)


def regles_du_jeu():
    running = True
    while running:
        FENETRE.blit(REGLES_BG, (0, 0))
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        retour.affiche(MENU)
        if quitter_menu.click(MENU):
            running = False
            main_menu()
        if retour.click(MENU):
            running = False
            main_menu()
        pygame.display.update()
        mainClock.tick(60)


def menu_classement():
    running = True
    while running:
        FENETRE.blit(CLASSMENT_BG, (0, 0))
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        retour.affiche(MENU)
        if quitter_menu.click(MENU):
            running = False
            main_menu()
        if retour.click(MENU):
            running = False
            main_menu()
        pygame.display.update()
        mainClock.tick(60)


def calcul(nb_cartes=32, nb_simulations=100):
    running = True
    main.graphique_probabilite(nb_cartes, nb_simulations)
    graph = pygame.transform.smoothscale(pygame.image.load(os.path.join(
        GRAPHIQUE_FOLDER, "graphique.png")).convert_alpha(), (x_ecran, y_ecran))
    while running:
        MENU.blit(MENU_BG, (0, 0))
        draw_text('calcul statistiques', font, (255, 255, 255), MENU, 20, 20)
        FENETRE.blit(graph, (0, 0))
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        retour.affiche(MENU)
        if quitter_menu.click(MENU):
            pygame.quit()
            sys.exit()
        if retour.click(MENU):
            running = False
            main_menu()
        pygame.display.update()
        mainClock.tick(60)


def param(nb_cartes=32, nb_tas_max=2):
    running = True
    while running:
        MENU.blit(MENU_BG, (0, 0))
        draw_text('parametre', font, (255, 255, 255), MENU, 20, 20)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        quitter_menu.affiche(MENU)
        lancer.affiche(MENU)
        mur.affiche(MENU)
        cartes_32.affiche(MENU)
        cartes_52.affiche(MENU)
        param_2.affiche(MENU)
        param_4.affiche(MENU)
        param_6.affiche(MENU)
        param_8.affiche(MENU)
        retour.affiche(MENU)
        logo.affiche(MENU)
        if quitter_menu.click(MENU):
            pygame.quit()
            sys.exit()
        if retour.click(MENU):
            main_menu()
        if cartes_32.click(MENU):
            nb_cartes = 32
        if cartes_52.click(MENU):
            nb_cartes = 52
        if param_2.click(MENU):
            nb_tas_max = 2
        if param_4.click(MENU):
            nb_tas_max = 4
        if param_6.click(MENU):
            nb_tas_max = 6
        if param_8.click(MENU):
            nb_tas_max = 8
        if lancer.click(MENU):
            game(nb_cartes, nb_tas_max)
        pygame.display.update()
        mainClock.tick(60)


START_GAME = True


def game(nb_cartes=32, nb_tas_max=2):
    global x_cartes, cartes_tire, nb_melanger, nb_aide, muter, nb_son
    run = True
    fps = 60
    vitesse_cartes = 5
    clock = pygame.time.Clock()
    muter = False
    gamePack(nb_cartes)

    def redraw():
        global muter, nb_melanger, nb_aide, nb_son
        FENETRE.blit(BG, (0, 0))
        draw_text('RDA Retro',
                  font, (255, 255, 255), FENETRE, 5, 5)
        draw_text(str(nb_melanger), font_nombre,
                  (255, 255, 255), FENETRE, (270/1440)*x_ecran, (839/900) *
                  y_ecran)
        draw_text(str(nb_aide), font_nombre,
                  (255, 255, 255), FENETRE, (570/1440)*x_ecran, (839/900) *
                  y_ecran)

        quitter.affiche(FENETRE)
        aide.affiche(FENETRE)
        avant.affiche(FENETRE)
        apres.affiche(FENETRE)
        open_menu.affiche(FENETRE)
        if open_menu.click(FENETRE):
            param()
        if apres.click(FENETRE):
            nb_son += 1
            nb_son = (nb_son % 3)+1
            mixer.music.load(os.path.join(
                SOUNDS_FOLDER, "bgmusic"+str(nb_son)+".mp3"))
            mixer.music.play(-1)
            mixer.music.set_volume(0.02)
        if avant.click(FENETRE):
            nb_son += 3
            nb_son = (nb_son % 3)+1
            mixer.music.load(os.path.join(
                SOUNDS_FOLDER, "bgmusic"+str(nb_son)+".mp3"))
            mixer.music.play(-1)
            mixer.music.set_volume(0.02)
        apres.affiche(FENETRE)
        melanger.affiche(FENETRE)
        if muter:
            mute.affiche(FENETRE)
        if not muter:
            unmute.affiche(FENETRE)
        if unmute.click(FENETRE):
            if muter == False:
                pygame.mixer.music.pause()
                muter = True
            else:
                pygame.mixer.music.unpause()
                muter = False
        open_menu.affiche(FENETRE)
        reussite_mode_manuel()
        for carte in game_pack[::-1]:
            carte.affiche(FENETRE)
        for carte in cartes_tire:
            carte.affiche(FENETRE)
            carte.move(vitesse_cartes, dir_carte)
        for c in game_pack:
            c.move(vitesse_cartes, dir_carte)
        if len(game_pack) != 0:
            pack.affiche(FENETRE)
        else:
            pack_vide.affiche(FENETRE)
        if len(game_pack) == 0 and aucun_saut(cartes_tire) == False:
            if len(cartes_tire) <= nb_tas_max:
                resultat(True)
            else:
                resultat(False)
        if quitter.click(FENETRE):
            main_menu()
        if melanger.click(FENETRE) and nb_melanger > 0:
            random.shuffle(game_pack)
            nb_melanger -= 1
        if aide.click(FENETRE) and nb_aide > 0:
            if ext_aide():
                nb_aide -= 1
        pygame.display.update()

    while run:
        clock.tick(fps)
        redraw()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False


if __name__ == "__main__":
    main_menu()
